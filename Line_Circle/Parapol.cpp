#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{

	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);

}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	//Area 1
	int p = 1 - A;
	int x = 0;
	int y = 0;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw2Points(xc, yc, x, y, ren);
	while (x < A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x + 3 - 2 * A;
			y += 1;
		}
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw2Points(xc, yc, x, y, ren);
	}
	//Area 2
	p = 2 * A - 1;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw2Points(xc, yc, x, y, ren);
	while (y + yc<100)
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			x++;
			p += 4 * A - 4 * x - 4;
		}
		y++;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw2Points(xc, yc, x, y, ren);
	}

}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	//Area 1
	int x = 0;
	int y = 0;
	int p = -1 - A;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw2Points(xc, yc, x, y, ren);
	while (x < A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			y -= 1;
			p += 2 * x + 3 - 2 * A;
		}
		x += 1;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw2Points(xc, yc, x, y, ren);
	}
	//Area 2
	p = 2 * A - 1;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw2Points(xc, yc, x, y, ren);
	while (y + yc > 0)
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			x++;
			p += 4 * A - 4 * x - 4;
		}
		y--;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw2Points(xc, yc, x, y, ren);
	}

}