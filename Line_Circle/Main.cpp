//#pragma region buoi3
////
////
////#include <iostream>
////#include <SDL.h>
////#include "Line.h"
////#include "Clipping.h"
////#include "DrawPolygon.h"
////#include "FillColor.h"
////
////using namespace std;
////
////const int WIDTH  = 800;
////const int HEIGHT = 600;
////
////
////int main(int, char**){
////	//First we need to start up SDL, and make sure it went ok
////	if (SDL_Init(SDL_INIT_VIDEO) != 0){
////		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
////		return 1;
////	}
////
////	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
////	//Make sure creating our window went ok
////	if (win == NULL){
////		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
////		return 1;
////	}
////
////	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
////    //DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE
////
////    SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
////	if (ren == NULL){
////		SDL_DestroyWindow(win);
////		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
////		SDL_Quit();
////		return 1;
////	}
////
////    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
////	SDL_RenderClear(ren);
////
////    //YOU CAN INSERT CODE FOR TESTING HERE
////
////    //Get pixel format of the window
////    Uint32 pixel_format = SDL_GetWindowPixelFormat(win);
////
////    SDL_Color fillColor = {255, 0, 0, 255};
////    SDL_Color boundaryColor = {0, 255, 0, 255};
////
////    //Draw a rectangle then fill it from startPoint by calling BoundaryFill4
////    SDL_Rect *rect1 = new SDL_Rect();
////    rect1->x = 1;
////    rect1->y = 1;
////    rect1->w = 100;
////    rect1->h = 200;
////    //Filling edges by boundary color
////    SDL_SetRenderDrawColor(ren, boundaryColor.r, boundaryColor.g, boundaryColor.b, boundaryColor.a);
////    SDL_RenderDrawRect(ren, rect1);
////
////    Vector2D startPoint(50, 50);
////    BoundaryFill4(win, startPoint, pixel_format, ren, fillColor, boundaryColor);
////
////    //Three vertices for triangle
////	Vector2D v1(200, 100);
////	Vector2D v2(150, 400);
////	Vector2D v3(400, 300);
////
////	//Filling triangle by calling TriangleFill
////    TriangleFill(v1, v2, v3, ren, fillColor);
////
////    SDL_RenderPresent(ren);
////
////    //Take a quick break after all that hard work
////    //Quit if happen QUIT event
////	bool running = true;
////
////	while(running)
////	{
////		SDL_Event e;
////		while(SDL_PollEvent(&e))
////		{
////			switch(e.type)
////			{
////				case SDL_QUIT: running = false; break;
////			}
////
////		}
////
////    }
////
////	SDL_DestroyRenderer(ren);
////	SDL_DestroyWindow(win);
////	SDL_Quit();
////
////	return 0;
////}
//#pragma endregion
//
//#pragma region buoi4
////
////#include <iostream>
////#include <SDL.h>
////#include "Line.h"
////#include "Clipping.h"
////#include "DrawPolygon.h"
////#include "FillColor.h"
////
////using namespace std;
////
////const int WIDTH = 800;
////const int HEIGHT = 600;
////
////
////int main(int, char**) {
////	//First we need to start up SDL, and make sure it went ok
////	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
////		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
////		return 1;
////	}
////
////	SDL_Window *win = SDL_CreateWindow("Hello World!", 50, 50, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
////	//Make sure creating our window went ok
////	if (win == NULL) {
////		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
////		return 1;
////	}
////
////	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
////	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE
////
////	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
////	if (ren == NULL) {
////		SDL_DestroyWindow(win);
////		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
////		SDL_Quit();
////		return 1;
////	}
////
////	// Set the blend mode to specify how the alpha channel is used
////	if (SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_BLEND) != 0) {
////		std::cout << "\"Could not set render draw blend mode: " << SDL_GetError() << std::endl;
////	}
////
////	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
////	SDL_RenderClear(ren);
////
////	//YOU CAN INSERT CODE FOR TESTING HERE
////
////	//Get pixel format of the window
////	Uint32 pixel_format = SDL_GetWindowPixelFormat(win);
////
////	//Set alpha channel = 255
////	SDL_Color fillColor = { 36, 113, 163, 255 };
////	SDL_Color boundaryColor = { 230, 126, 34, 255 };
////
////	//Draw a rectangle then fill it from startPoint by calling BoundaryFill4
////	SDL_Rect *rect1 = new SDL_Rect();
////	rect1->x = 1;
////	rect1->y = 1;
////	rect1->w = 10;
////	rect1->h = 10;
////	//Filling edges by boundary color
////	SDL_SetRenderDrawColor(ren, boundaryColor.r, boundaryColor.g, boundaryColor.b, boundaryColor.a);
////	SDL_RenderDrawRect(ren, rect1);
////
////	Vector2D startPoint(5, 5);
////	BoundaryFill4(win, startPoint, pixel_format, ren, fillColor, boundaryColor);
////
////	//Three vertices for triangle
////	Vector2D v1(200, 100);
////	Vector2D v2(150, 400);
////	Vector2D v3(400, 300);
////
////	//Filling triangle by calling TriangleFill
////	//TriangleFill(v1, v2, v3, ren, fillColor);
////
////	int xc = 200;
////	int yc = 200;
////	int R = 100;
////	
////
////	FillIntersectionRectangleCircle(v1, v2, xc, yc, R, ren, fillColor);
////
////	SDL_RenderPresent(ren);
////
////	//Take a quick break after all that hard work
////	//Quit if happen QUIT event
////	bool running = true;
////
////	while (running)
////	{
////		SDL_Event e;
////		while (SDL_PollEvent(&e))
////		{
////			switch (e.type)
////			{
////			case SDL_QUIT: running = false; break;
////			}
////
////		}
////
////	}
////
////	SDL_DestroyRenderer(ren);
////	SDL_DestroyWindow(win);
////	SDL_Quit();
////
////	return 0;
////}
////

//#pragma endregion

//#pragma region buoi4_phan2
//#include <iostream>
//#include <SDL.h>
//#include "Bezier.h"
//#include "Circle.h"
//
//using namespace std;
//
//const int WIDTH = 800;
//const int HEIGHT = 1000;
//SDL_Window* window = NULL;
//SDL_Renderer* ren = NULL;
//
//
//bool CheckMouseClick(SDL_Rect *rect, int x, int y)
//{
//	if ((x >= rect->x) && (x <= (rect->x + rect->w)) && (y >= rect->y) && (y <= (rect->y + rect->h)))
//		return true;
//	else
//		return false;
//}
//
//void veDiem(Vector2D p1, Vector2D p2,SDL_Renderer *ren,SDL_Rect* rect)
//{
//	SDL_SetRenderDrawColor(ren, 0, 255, 40, 255);
//	SDL_RenderDrawRect(ren, rect);
//	SDL_RenderDrawLine(ren, p1.x, p1.y, p2.x, p2.y);
//}
//
//int main(int, char**) {
//	//First we need to start up SDL, and make sure it went ok
//	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
//		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
//		return 1;
//	}
//
//	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
//	//Make sure creating our window went ok
//	if (win == NULL) {
//		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
//		return 1;
//	}
//
//	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
//	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE
//
//	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
//	if (ren == NULL) {
//		SDL_DestroyWindow(win);
//		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
//		SDL_Quit();
//		return 1;
//	}
//
//	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
//	SDL_RenderClear(ren);
//
//	//YOU CAN INSERT CODE FOR TESTING HERE
//
//	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);
//
//
//	SDL_Rect *rect1 = new SDL_Rect();
//	rect1->x = p1.x - 3;
//	rect1->y = p1.y - 3;
//	rect1->w = 6;
//	rect1->h = 6;
//
//	SDL_Rect *rect2 = new SDL_Rect();
//	rect2->x = p2.x - 3;
//	rect2->y = p2.y - 3;
//	rect2->w = 6;
//	rect2->h = 6;
//
//	SDL_Rect *rect3 = new SDL_Rect();
//	rect3->x = p3.x - 3;
//	rect3->y = p3.y - 3;
//	rect3->w = 6;
//	rect3->h = 6;
//
//	SDL_Rect *rect4 = new SDL_Rect();
//	rect4->x = p4.x - 3;
//	rect4->y = p4.y - 3;
//	rect4->w = 6;
//	rect4->h = 6;
//
//	SDL_Color colorCurve = { 100, 20, 40, 255 }, colorRect = { 0, 255, 40, 255 };
//
//	SDL_SetRenderDrawColor(ren, colorRect.r, colorRect.g, colorRect.b, 20);
//	SDL_RenderDrawRect(ren, rect1);
//	SDL_RenderDrawRect(ren, rect2);
//	SDL_RenderDrawRect(ren, rect3);
//	SDL_RenderDrawRect(ren, rect4);
//	SDL_RenderDrawLine(ren, p1.x, p1.y, p2.x, p2.y);
//	SDL_RenderDrawLine(ren, p3.x, p3.y, p2.x, p2.y);
//	SDL_RenderDrawLine(ren, p3.x, p3.y, p4.x, p4.y);
//
//
//	SDL_SetRenderDrawColor(ren, colorCurve.r, colorCurve.g, colorCurve.b, colorCurve.a);
//	//DrawCurve2(ren, p1, p2, p3);
//	DrawCurve3(ren, p1, p2, p3, p4);
//
//	SDL_RenderPresent(ren);
//	//Take a quick break after all that hard work
//	//Quit if happen QUIT event
//	bool running = true;
//	
//	while (running)
//	{
//		SDL_Event event;
//		//If there's events to handle
//		if (SDL_PollEvent(&event))
//		{
//			if (event.type == SDL_MOUSEBUTTONDOWN)
//			{
//				bool run = true;
//				while (run)
//				{
//					SDL_Event event1;
//					while (SDL_PollEvent(&event1))
//					{
//						if (event1.type == SDL_MOUSEBUTTONUP)
//						{
//							run = false;
//						}
//						if (event1.type == SDL_MOUSEMOTION)
//						{
//							int x, y;
//							SDL_GetMouseState(&x, &y);
//							Vector2D p;
//							p.x = x;
//							p.y = y;
//							if (CheckMouseClick(rect1, x, y)==true)
//							{
//								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
//								SDL_RenderClear(ren);
//								p1 = p;
//								SDL_SetRenderDrawColor(ren, colorCurve.r, colorCurve.g, colorCurve.b, colorCurve.a);
//								DrawCurve3(ren, p1, p2, p3, p4);
//								rect1->x = p1.x - 3;
//								rect1->y = p1.y - 3;
//								rect1->w = 6;
//								rect1->h = 6;
//								veDiem(p1, p2, ren, rect1);
//								veDiem(p2, p3, ren, rect2);
//								veDiem(p3, p4, ren, rect3);
//								SDL_RenderDrawRect(ren, rect4);
//								SDL_RenderPresent(ren);
//							}
//							if (CheckMouseClick(rect2, x, y)==true)
//							{
//								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
//								SDL_RenderClear(ren);
//								p2 = p;
//								SDL_SetRenderDrawColor(ren, colorCurve.r, colorCurve.g, colorCurve.b, colorCurve.a);
//								DrawCurve3(ren, p1, p2, p3, p4);
//								rect2->x = p2.x - 3;
//								rect2->y = p2.y - 3;
//								rect2->w = 6;
//								rect2->h = 6;
//								veDiem(p1, p2, ren, rect1);
//								veDiem(p2, p3, ren, rect2);
//								veDiem(p3, p4, ren, rect3);
//								SDL_RenderDrawRect(ren, rect4);
//								SDL_RenderPresent(ren);
//							}
//							if (CheckMouseClick(rect3, x, y)==true)
//							{
//								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
//								SDL_RenderClear(ren);
//								p3 = p;
//								SDL_SetRenderDrawColor(ren, colorCurve.r, colorCurve.g, colorCurve.b, colorCurve.a);
//								DrawCurve3(ren, p1, p2, p3, p4);
//								rect3->x = p3.x - 3;
//								rect3->y = p3.y - 3;
//								rect3->w = 6;
//								rect3->h = 6;
//								veDiem(p1, p2, ren, rect1);
//								veDiem(p2, p3, ren, rect2);
//								veDiem(p3, p4, ren, rect3);
//								SDL_RenderDrawRect(ren, rect4);
//								SDL_RenderPresent(ren);
//							}
//							if (CheckMouseClick(rect4, x, y)==true)
//							{
//								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
//								SDL_RenderClear(ren);
//								p4 = p;
//								SDL_SetRenderDrawColor(ren, colorCurve.r, colorCurve.g, colorCurve.b, colorCurve.a);
//								DrawCurve3(ren, p1, p2, p3, p4);
//								rect4->x = p4.x - 3;
//								rect4->y = p4.y - 3;
//								rect4->w = 6;
//								rect4->h = 6;
//								veDiem(p1, p2, ren, rect1);
//								veDiem(p2, p3, ren, rect2);
//								veDiem(p3, p4, ren, rect3);
//								SDL_RenderDrawRect(ren, rect4);
//								SDL_RenderPresent(ren);
//							}
//						}
//					}
//				}
//			}
//
//			//If the user has Xed out the window
//			if (event.type == SDL_QUIT)
//			{
//				//Quit the program
//				running = false;
//			}
//		}
//
//	}
//
//	SDL_DestroyRenderer(ren);
//	SDL_DestroyWindow(win);
//	SDL_Quit();
//
//	return 0;
//}
//
//#pragma endregion


#pragma region test

#include <iostream>
#include <SDL.h>
#include "Line.h"
#include "Circle.h"
#include "Ellipse.h"
#include "Parapol.h"
#include "Clipping.h"
#include "DrawPolygon.h"

#include "Vector2D.h"
#include "Matrix2D.h"

int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}
	//Now create a window with title "Hello World" at 10, 10 on the screen with w:800 h:600 and show it
	SDL_Window *win = SDL_CreateWindow("Hello World!", 10, 10, 800, 600, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == nullptr) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//video driver supports the flags we're passing
	//Flags: SDL_RENDERER_ACCELERATED: We want to use hardware accelerated rendering
	//SDL_RENDERER_PRESENTVSYNC: We want the renderer's present function (update screen) to be
	//synchronized with the monitor's refresh rate
	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == nullptr) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	//int a, b, A, xc, yc;
	//xc = 200;
	//yc = 200;
	//a = 100;
	//b = 200;
	//A = 50;

	//SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);
	//BresenhamDrawParapolNegative(xc, yc, A, ren);

	//SDL_SetRenderDrawColor(ren, 255, 43, 226, 255);
	//MidPointDrawEllipse(xc, yc, a, b, ren);

	////TEST BRESENHAM LINE
	//
	////SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);
	////Bresenham_Line(400, 300, 600, 400, ren); // case 1
	////Bresenham_Line(400, 300, 200, 200, ren); // case 2
	////Bresenham_Line(400, 300, 500, 500, ren); // case 3
	////Bresenham_Line(400, 300, 300, 100, ren); // case 4
	////Bresenham_Line(400, 300, 600, 200, ren); // case 5
	////Bresenham_Line(400, 300, 200, 400, ren); // case 6
	////Bresenham_Line(400, 300, 300, 500, ren); // case 7
	////Bresenham_Line(400, 300, 500, 100, ren); // case 8

	////SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	////Bresenham_Line(400, 100, 400, 500, ren); // oy
	////Bresenham_Line(200, 100, 200, 500, ren); // oy
	////Bresenham_Line(600, 100, 600, 500, ren); // oy
	////Bresenham_Line(200, 300, 600, 300, ren); // ox
	////Bresenham_Line(200, 500, 600, 500, ren); // ox
	////Bresenham_Line(200, 100, 600, 100, ren); // ox
	//

	////TEST BRESENHAM LINE
	//
	//SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);
	//Midpoint_Line(400, 300, 600, 400, ren); // case 1
	//Midpoint_Line(400, 300, 200, 200, ren); // case 2
	//Midpoint_Line(400, 300, 500, 500, ren); // case 3
	//Midpoint_Line(400, 300, 300, 100, ren); // case 4
	//Midpoint_Line(400, 300, 600, 200, ren); // case 5
	//Midpoint_Line(400, 300, 200, 400, ren); // case 6
	//Midpoint_Line(400, 300, 300, 500, ren); // case 7
	//Midpoint_Line(400, 300, 500, 100, ren); // case 8

	//SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	//Midpoint_Line(400, 100, 400, 500, ren); // oy
	//Midpoint_Line(200, 100, 200, 500, ren); // oy
	//Midpoint_Line(600, 100, 600, 500, ren); // oy
	//Midpoint_Line(200, 300, 600, 300, ren); // ox
	//Midpoint_Line(200, 500, 600, 500, ren); // ox
	//Midpoint_Line(200, 100, 600, 100, ren); // ox
	//




	//Vector2D v1(0, 0);
	//Vector2D v2(100, 100);
	//Vector2D v3(v1), v4(v2);

	////m2: Matrix for transform axis
	//Matrix2D m2;
	////m3: Matrix for transform line
	//Matrix2D m3;

	/*SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	Bresenham_Line(v1.x, v1.y, v2.x, v2.y, ren);

	v3.set(m2.mul(v3).x, m2.mul(v3).y);
	v4.set(m2.mul(v4).x, m2.mul(v4).y);

	SDL_SetRenderDrawColor(ren, 34, 139, 34, 255);
	Midpoint_Line(v3.x, v3.y, v4.x, v4.y, ren);*/


	int xc = 200;
	int yc = 200;
	int R = 100;

	int left = 300;
	int right = 400;
	int top = 100;
	int bottom = 600;

	RECT r = CreateWindow(left, right, top, bottom);

	Vector2D P1(400, 200);
	Vector2D P2(100, 700);
	Vector2D Q1, Q2;


	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	//Bresenham_Line(P1.x, P1.y, P2.x, P2.y, ren);


	CohenSutherland(r, P1, P2, Q1, Q2);

	//LiangBarsky(r, P1, P2, Q1, Q2);

	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	Bresenham_Line(Q1.x, Q1.y, Q2.x, Q2.y, ren);


	SDL_RenderPresent(ren);
	SDL_Delay(15000);

	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;
	double degrees = 0;
	double angle;

	while (running)
	{
		SDL_Event e;
		while (SDL_PollEvent(&e))
		{
			switch (e.type)
			{
			case SDL_QUIT: running = false; break;
			case SDL_KEYDOWN:
			case SDL_KEYUP:
				//INSERT YOUR CODE HERE

				SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
				SDL_RenderClear(ren);

				SDL_SetRenderDrawColor(ren, 184, 134, 11, 255);
				//Bresenham_Line(v3.x, v3.y, v4.x, v4.y, ren);

				SDL_RenderPresent(ren);

				break;
			}

		}

	}
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}


#pragma endregion


